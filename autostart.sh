# autostart file for miza installation

killall -9 /usr/lib/ardour7/ardour-7.3.0~ds0

killall -9 liquidsoap qjackctl jackd sclang scide;

barrier &

# choose audio interface
#/usr/bin/jackd  -aa -u -dalsa -r48000 -p256 -n3 -D -Chw:U192k -Phw:U192k &
usr/bin/jackd  -aa -u -dalsa -r44100 -p256 -n3 -D -Chw:VSL -Phw:VSL &

sleep 4;

qjackctl  -a ~/witw/witw.xml &

sleep 2;

scide ~/witw/sc/witw.scd &

#sleep 1;

ardour ~/witw/ardour/witw/witw.ardour &

#sleep 1;

liquidsoap ~/witw/witw.liq &

#xterm -e "liquidsoap  ~/spellcaster/spellcaster.liq "

#sleep 1;

xterm; &
