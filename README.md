# Whistling in the Wind

#### A 'data-guided' generative music composition by Rob Canning (2023-24)

Whistling in the Wind is an installation combining electronic music with real-time data sonification.  The piece takes live and histoic data from environmental monitoring stations as its data inputs which then influences musical change in real-time within the composition's own generative logic. In this sense it is more of a 'data-guided' generative music composition than a direct sonification of incoming data. 

The installation can be thought of as similar to wind chimes in that one system (the collection of tuned pipes) is exposed to another (the wind speed and direction), but a little more complex. The first system is the musical composition, described in software, where some parameters are variable and  open to external influence while other element are fixed and more deliberatly composed. 

For example, some of the rules of the work function as follows, if the wind gusts come from the east then a melodic minor scale becomes dominant, unless the temperature is below 10 Celcius in which case it becomes a natural minor. If the wind is from the west then the composition uses more chromatic melodic elements, the variation between the wind gusts and the average wind speed influence the tempi and pace of the music. Relative humidity exerts influence of the density of rhythmic activity and sustain length of the notes. If the humidity is high in combination with high temperatures then the registral charachter of the music becomes more shrill moving activity into the upper octaves of the virtual instrument. Rainfall triggers the insertion of shorter more pontalist rhythmic fragments within the soundscape. These are the some of possible examples of how the music and data interact with one another to create the evolving soundscape of *Whistling in the Wind*

## Installation and Live Stream

The sound installation will be installed in a physical location such as a gallery or public space in Maribor in 2024.  *Whistling in the Wind*  currently exists as existing as a live streaming Internet radio station. 

- [http://rizom.si:8000/whistling_in_the_wind.mp3](http://rizom.si:8000/whistling_in_the_wind.mp3)

## Credits

*Whistling in the Wind* was commissioned through the EU funded KonS Platform for Contemporary Investigative Arts as a creative response within the city{making}sense project directed by urban planner and architect Andrej Žižek. [https://kons-platforma.org/dogodki/andrej-zizek-citymakingsense/()](https://kons-platforma.org/dogodki/andrej-zizek-citymakingsense) It is producted as a co-production with Zavod Rizoma [http://zavodrizoma.si/](http://zavodrizoma.si/)

The environmental monitoring stations currently used by the work are located in the city of Maribor in Slovenia and are developed and maintained by Institute IRNAS [https://www.irnas.eu/](https://www.irnas.eu).


## Technologies

The composition uses Supercollider in conjuntion with a set of Python and Bash scripts which gather data from the environmental monitoring stations via an API. The composition is then streamed as an online radio using Liquidsoap and Icecast. Everything runs headless on a Debian 12 server requiring minimal long term maintenance.

### Code
The code lives in a git reposity publicly hosted by kompot.si, a collective of Slovenia's finest autonomous infrastructure and free software advocates.
- [https://git.kompot.si/rob/witw](https://git.kompot.si/rob/witw)

### System Requirments

Debian 12 OS, Jack2, Supercollider 3.13.0, Python3, Bash, Liquidsoap, Icecast2 Streaming Server

----

